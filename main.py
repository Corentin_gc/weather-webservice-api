import web
import json
import os
from dotenv import load_dotenv
import httplib2
import sqlite3
import datetime


# Loading file
load_dotenv()
api_key = os.getenv("OPENWEATHERMAP_KEY")
# Generating routes
urls = (
    '/weather(.*)', 'weather',
)
app = web.application(urls, globals())


class weather:
    def GET(self, zip):
        zipcode = web.input(_method='get').zipcode
        if len(zipcode) == 5 :
            self.createDatabase()
            if self.selectWeather(zipcode) == None:
                return json.dumps(self.requestToApi(zipcode))
            else:
                return json.dumps(self.dataToJson(self.selectWeather(zipcode)))
        else:
            return "Mauvais code postal ! Merci d'en rentrer un valide"

    def k_to_c(self, temperature):
        return temperature - 273.15

    def createDatabase(self):
        self.conn = sqlite3.connect('ma_base.db')
        self.cursor = self.conn.cursor()
        self.cursor.execute("""CREATE TABLE IF NOT EXISTS weather(
            id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
            actualTemp TEXT,
            minTemp TEXT,
            maxTemp TEXT,
            meteo TEXT,
            zipcode TEXT,
            updateDate timestamp 
        )""")
        self.conn.commit()

    def selectWeather(self, zipcode):
        self.cursor.execute(f"SELECT * FROM weather WHERE zipcode =={zipcode}")
        weather = self.cursor.fetchone()
        return weather

    def requestToApi(self, zipcode):
        countryCode = "FR"
        request_api = f"http://api.openweathermap.org/data/2.5/weather?zip={zipcode},{countryCode}&appid={api_key}"
        h = httplib2.Http('.cache')
        response, content = h.request(request_api,headers={'cache-control': 'no-cache', 'Content-Type': 'application/json'})
        if response['status'] != "404" :
            json_content = json.loads(content)
            json_result = {
                "actualTemp": self.k_to_c(json_content["main"]["temp"]),
                "minTemp": self.k_to_c(json_content["main"]["temp_min"]),
                "maxTemp": self.k_to_c(json_content["main"]["temp_max"]),
                "meteo": json_content["weather"][0]["main"],
                "zipcode": zipcode,
                "updateDate": (datetime.datetime.now()).timestamp(),
            }
            self.insertDatabase(json_result)
            return json_result
        else:
            return "Une erreur est survenu ou vous avez entrez un mauvais code postale"
    def insertDatabase(self, data):
        self.cursor.execute("INSERT INTO weather(actualTemp, minTemp, maxTemp, meteo, zipcode, updateDate) VALUES(:actualTemp, :minTemp, :maxTemp, :meteo, :zipcode, :updateDate)", data)
        self.conn.commit()

    def dataToJson(self, data):
        table_info = self.cursor.execute("PRAGMA table_info(weather)").fetchall()
        response={}
        for column in table_info:
            response[column[1]]= data[column[0]]
        if response["updateDate"] + 900 < (datetime.datetime.now()).timestamp():
            self.deleteInDatabase(response)
            response = self.requestToApi(response['zipcode'])
            return response
        else:
            return response

    def deleteInDatabase(self, data):
        self.cursor.execute(f"DELETE FROM weather WHERE zipcode = {data['zipcode']}")
        self.conn.commit()


if __name__ == "__main__":
    app.run()